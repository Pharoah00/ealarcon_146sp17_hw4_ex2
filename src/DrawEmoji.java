import java.awt.Color;
import java.awt.Graphics;
import java.awt.Polygon;

import javax.swing.JPanel;

/**
 * @author Eduardo Alarcon
 * @version Homework 4 Exercise 2: Draw an Emoji
 */
public class DrawEmoji  extends JPanel
{
	private final static Color BROWN = new Color(90,56,18);
	public void paintComponent( Graphics a) 
	{
		super.paintComponent(a);
		
		// Draw poop's core
		a.setColor(BROWN);
		//                          x    y   x-axis           y-axis  y-axis  y-axis
		//						   Tip  Tip L-Corner           Top    R-Side  L-Side
		a.fillPolygon( new int[] { 225, 400,   50}, new int[] { 100,    350,    350 }, 3);
		
		// Bottom Curve of Left Part of Swirl
		a.fillArc( 33, 265, 190, 105, 100, 225 );

		// Second Curve up of Left Part of Swirl
		a.fillArc( 90, 200, 150, 80, 50, 190 );
		
		// Third Curve up of Left Part of Swirl
		a.fillArc( 140, 150, 90, 60, 50, 190 );
		
		// Top part of the triangle
		// Shrink it 
		//                          x    y   x-axis           y-axis  y-axis  y-axis
		//						   Tip  Tip L-Corner           Top    R-Side  L-Side
		a.fillPolygon( new int[] { 205, 215, 227}, new int[] { 104,    115,    100 }, 3);
		
		//Bottom Curve of Right Part of Swirl
		a.fillArc( 273, 270, 150, 87, -50, 225 );
		
		// Second Curve up of Right Part of Swirl
		a.fillArc( 270, 210, 100, 70, -70, 180 );
		
		// Third Curve up of Right Part of Swirl
		a.fillArc( 230, 160, 90, 60, -50, 190 );
		
		// My attempt to make curved swirl at the bottom
		a.setColor(Color.white);
		a.fillArc( 180, 345, 75, 50, 0, 180 );
		
		// Mouth
		a.fillArc( 160, 240, 150, 80, 180, 180 );
		
		// Left Eye
		a.fillOval(150, 190, 55, 75);
		
		// Right Eye
		a.fillOval(250, 190, 55, 75);
		
		// Pupil for Left Eye
		a.setColor(Color.BLACK);
		a.fillOval(157, 195, 40, 60);
		
		// Pupil for Right Eye
		a.fillOval(257, 195, 40, 60);
				
		
	} //end main method

} //end DrawEmoji class
/*
//Fig. 6.11: DrawSmiley.java
//Demonstrates filled shapes.
import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JPanel;

public class DrawSmiley extends JPanel
{
public void paintComponent(Graphics g)
{
   super.paintComponent(g);

   // draw the face
   g.setColor(Color.YELLOW);
   g.fillOval(10, 10, 200, 200);
   
   // draw the eyes
   g.setColor(Color.BLACK);
   g.fillOval(55, 65, 30, 30);
   g.fillOval(135, 65, 30, 30);
   
   // draw the mouth
   g.fillOval(50, 110, 120, 60);
   
   // "touch up" the mouth into a smile
   g.setColor(Color.YELLOW);
   g.fillRect(50, 110, 120, 30);
   g.fillOval(50, 120, 120, 40);
} 
} // end class DrawSmiley


/**************************************************************************
* (C) Copyright 1992-2014 by Deitel & Associates, Inc. and               *
* Pearson Education, Inc. All Rights Reserved.                           *
*                                                                        *
* DISCLAIMER: The authors and publisher of this book have used their     *
* best efforts in preparing the book. These efforts include the          *
* development, research, and testing of the theories and programs        *
* to determine their effectiveness. The authors and publisher make       *
* no warranty of any kind, expressed or implied, with regard to these    *
* programs or to the documentation contained in these books. The authors *
* and publisher shall not be liable in any event for incidental or       *
* consequential damages in connection with, or arising out of, the       *
* furnishing, performance, or use of these programs.                     *
*************************************************************************/
